const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
 
const adapter = new FileSync('./model/data/db.json');
const db = low(adapter);

const uuidv1 = require('uuid/v1')
//const uuidv4 = require('uuid/v4')

exports.getUuid = function () {
    return uuidv1();
}

exports.findAll = function (collection) {
    return db.get(collection).value();
}

exports.search = function (collection, filter) {
    return db.get(collection).filter(filter).value();
}

exports.getById = function (collection, id) {
    return db.get(collection).find({"id":id}).value();
}

exports.save = function (collection, entity) {
    if (entity.id==undefined || entity.id==null || entity.id=='') {
        entity.id=this.getUuid();
        db.get(collection)
            .push(entity)
            .write();
    } else {
        db.get(collection)
            .find({id: entity.id })
            .assign(entity)
            .write();
    }
}

exports.delete = function (collection, id) {
    db.get(collection)
        .remove({"id": id })
        .write();
}