var querystring = require('querystring');
var dao = require("./dao");
var productService = require("./productService");
var customerService = require("./customerService");

exports.getEntity = function (requestParams, operation, userInfo, nextViewPath, request)  {

  var invoice = new Object();

  var invoiceAttributes = ["id", "customerId", "newCustomer", "total", "date", "paye", "reste"];

  for(var i=0; i<invoiceAttributes.length; i++) {
    var param = invoiceAttributes[i];
    if (requestParams[param]!=undefined && requestParams[param]!=null) {
      invoice[param]=requestParams[param];
    }
  }

  var i=0;
  var attribute = "invoiceProducts";
  invoice[attribute] = new Array();
  //console.log("---->>>>>>invoiceProducts"+i+".quantity :"+requestParams[attribute+i+".quantity"]);
  while (requestParams[attribute+i+".productId"]!=undefined) {
    var item = {
      productId : requestParams[attribute+i+".productId"],
      quantity : requestParams[attribute+i+".quantity"],
      price : requestParams[attribute+i+".price"],
      total : requestParams[attribute+i+".total"]
    }

    console.log("***push from request");
    invoice[attribute].push(item);
    i++;
  }

  return invoice;
};

exports.initEntityForDisplay = function (invoice, requestParams, operation, userInfo, nextViewPath, request) {
  if (nextViewPath.indexOf("invoiceEdit")>=0) {
    invoice["customers"] = dao.findAll("customers");
    invoice["products"] = dao.findAll("products");
    
  } else if (nextViewPath.indexOf("invoiceList")>=0) {

    var customerMap = new Object();
    var customerList = customerService.findAllCustomers();
    for (var index = 0; index < customerList.length; index++) {
      customerMap[customerList[index].id] = customerList[index];
    }

    var productMap = new Object();
    var productList = productService.findAllProducts();
    for (var index = 0; index < productList.length; index++) {
      productMap[productList[index].id] = productList[index];
    }

    for (var index = 0; index < invoice.length; index++) {
        invoice[index]["customer"] = customerMap[invoice[index].customerId];

        //invoice[index]["dateString"] = (new Date(invoice[index]["date"])).toString('dd-mm-yyyy');

        for (let j = 0; j < invoice[index]["invoiceProducts"].length; j++) {
          const elt = invoice[index]["invoiceProducts"][j];
          elt["product"] = productMap[elt.productId];
        }
    }

  }
};

exports.addInvoiceProduct = function (product, requestParams, operation, userInfo, nextViewPath, request) {
  var item = {
    productId : "",
    quantity : 1,
    price : 0
  }
  console.log("***push from add");
  product["invoiceProducts"].push(item);

  return product;
}

exports.saveInvoice = function (invoice, requestParams, operation, userInfo, nextViewPath, request) {
  if (invoice.customerId=='new.customer' && invoice.newCustomer!=null) {
    console.log("****!!!saving customer before invoice");
    var customer = new Object();
    customer.name = invoice.newCustomer;
    dao.save("customers", customer);
    invoice.customerId = customer.id;
    invoice.newCustomer = undefined;
  }
  
  dao.save("invoices", invoice);
  return invoice;
};

exports.findAllInvoices = function (invoice, requestParams, operation, userInfo, nextViewPath, request) {
  return dao.findAll("invoices");
};

exports.editInvoice = function (invoice, requestParams, operation, userInfo, nextViewPath, request) {
  return dao.getById("invoices", requestParams['id']);
};

