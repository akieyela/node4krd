var querystring = require('querystring');
var dao = require("./dao");

exports.getEntity = function (requestParams, operation, userInfo, nextViewPath, request)  {

  var customer = new Object();
  var customerAttributes = ["id", "name", "address"];

  // on récupère les informations soumise par l'utilisateur
  for(var i=0; i<customerAttributes.length; i++) {
  var customerAttributes = ["id", "name", "address"];
    var param = customerAttributes[i];
    if (requestParams[param]!=undefined && requestParams[param]!=null) {
      customer[param]=requestParams[param];
    }
  }
  //
  return customer;
};

exports.initEntityForDisplay = function (customer, requestParams, operation, userInfo, nextViewPath, request) {
  
};

exports.saveCustomer = function (customer, requestParams, operation, userInfo, nextViewPath, request) {
  dao.save("customers", customer);
  return customer;
};

exports.findAllCustomers = function (customer, requestParams, operation, userInfo, nextViewPath, request) {
  return dao.findAll("customers");
};
