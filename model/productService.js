var url = require("url");
var querystring = require('querystring');
var dao = require("./dao");

exports.getEntity = function (requestParams, operation, userInfo, nextViewPath, request)  {

  var product = new Object();

  var productAttributes = ["id", "name", "price"];

  for(var i=0; i<productAttributes.length; i++) {
    var param = productAttributes[i];
    //console.log(">>>>>>>"+param+":"+requestParams[param]);
    if (requestParams[param]!=undefined && requestParams[param]!=null) {
      product[param]=requestParams[param];
    }
  }

  return product;
};

exports.initEntityForDisplay = function (product, requestParams, operation, userInfo, nextViewPath, request) {
  
};

exports.saveProduct = function (product, requestParams, operation, userInfo, nextViewPath, request) {
  dao.save("products", product);
  return product;
};

exports.findAllProducts = function (product, requestParams, operation, userInfo, nextViewPath, request) {
  return dao.findAll("products");
};
