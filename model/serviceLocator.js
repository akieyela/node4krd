var invoiceService = require("./invoiceService");
var productService = require("./productService");
var customerService = require("./customerService");

exports.getService = function (requestParams, operation, userInfo, nextViewPath, request) {
    var service = null;
    
    if (request.url.indexOf("/invoice")>=0) {
      service = invoiceService;
    } else if (request.url.indexOf("/product")>=0) {
      service = productService;
    } else if (request.url.indexOf("/customer")>=0) {
      service = customerService;
    }
  
    return service;
};