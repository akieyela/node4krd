var serviceLocator = require("./serviceLocator");
var dao = require("./dao");

exports.performOperation = function (requestParams, operation, userInfo, nextViewPath, request) {
  var view = new Object();
  var entity = new Object();

  // détermination du servcie à utiliser
  var service = serviceLocator.getService(requestParams, operation, userInfo, nextViewPath, request);

  // ici on récupère l'entité soumise par l'utilisateur au travers d'un formulaire d'édition
  if (request.url.indexOf("Edit")>=0) {
    entity = service["getEntity"](requestParams, operation, userInfo, nextViewPath, request);
  } 

  // exécution de l'opération demander par l'utilisateur : 
  try {
    entity = service[operation](entity, requestParams, operation, userInfo, nextViewPath, request);
  } catch (error) {
    console.log("*******ERROR!!!!! :"+error);
    console.log("******* operation:"+operation);
  }

  // initialisation de l'objet view
  view["model"] = entity;

  // Intialisation des listes de l'entité pour l'affichage
  try {
    service["initEntityForDisplay"](entity, requestParams, operation, userInfo, nextViewPath, request);
  } catch (error) {
    console.log("*******ERROR!!!!! :"+error);
  }

  // on a toujours besoin du menu
  view["menuitems"] = dao.findAll("menuitems");

  return view;
};
