exports.getMenuModel = function (request) {
  var menu = [
      {path:"/view/product/productList", label : "Liste de produits"}, 
      {path:"/view/product/newProduct", label : "Ajouter un produit"}, 
      {path:"/view/invoice/customerList", label : "Liste des clients"},
      {path:"/view/invoice/newCustomer", label : "Nouveau client"},
      {path:"/view/invoice/invoiceList", label : "Liste des factures"},
      {path:"/view/invoice/newInvoice", label : "Nouvelle  vente"},
      {path:"/view/book/editBook", label : "Edit BOOK"},
      {path:"/newBook/view/book/editBook?action=create", label : "Create BOOK"}
    ];
  return menu;
};