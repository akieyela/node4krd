var http = require('http');
var url = require("url");
var querystring = require('querystring');
var controller = require("./controller/controller");

http.createServer(function (request, response) {

  if (request.method == 'POST') {
    var body = '';
    request.on('data', function (data) {
        body += data;
        //console.log("Partial body: " + body);
    });

    request.on('end', function () {
        console.log("Body: " + body);
        var requestParams = querystring.parse(body);
        controller.handleRequest(requestParams, request, response);
    });
  } else {
    var requestParams = url.parse(request.url, true).query;
    controller.handleRequest(requestParams, request, response);
  }
    
}).listen(9090); 


/*console.log('*** contexte : '+request.url);

  console.log('*** year : '+q.query.year);
  console.log('*** month : '+q.query.month);

  console.log('*** host : '+q.host);
  console.log('*** pathname : '+q.pathnqme);
  console.log('*** search : '+q.sezarch);*/



/*fs.readdir("./", (err, files) => {
  files.forEach(file => {
    console.log(file);
  });
});*/

  //response.write("<html><body><h1>Hello Querido ! </h1> <p> Date du jour : "+dt.myDateTime()+"</p></body></html>");
  //response.end();
  //response.end('Hello World - updated 3 !');