#### Guide d'installation
1) Installer Node
2) Avec un terminal, se déplacer dans le dossier du projet (là ou se trouve start-app.js)
3) Exécuter les commandes :
	a) npm install --save handlebars
	b) npm install --save mime-types
	c) npm install lowdb
	d) npm install uuid
4) Lancer l'application : node start-app.js
5) Ouvir un navigateur et entrer l'adresse : http://localhost:9090