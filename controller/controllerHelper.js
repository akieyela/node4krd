var viewHelper = require('../view/viewHelper'); // sans le .js!

exports.extractNextViewPath = function (request) {
  var path = request.url;

  var aux =  path.indexOf("?");
  if (aux>0) {
    path = path.substring(0, aux);
  }

  if (path.length == 1) { // contexte racine
    path = "/index.html";
  }

  aux =  path.indexOf("/view/");
  if (aux >= 0) {
    path = path.substring(aux);
  }

  if (path.indexOf(".")<0) {
    path += ".html";
  }

  return path;
};

exports.extractUserOperation = function (request) {
  var operation = request.url;

  var aux =  operation.indexOf("?");
  if (aux>0) {
    operation = operation.substring(0, aux);
  }

  if (operation.length==1 || operation=="/index" || operation=="/index.html") { // contexte racine
    return null;
  }

  var aux =  operation.indexOf("/view/");

  if (aux > 0) {
    operation = operation.substring(1, aux);
  }

  return operation;
};

exports.getUserSentInfo = function (request, response) {
  // code ici

  return null;
};


exports.display404ErrorView = function (response) {
  // Testing if a file does not exist
  response.writeHead(404, { 'Content-Type': 'text/plain' });
  response.end();
};

exports.displayView = function (viewPath, contextObject, response) {
    viewHelper.generateView(response, contextObject, "./view/layout.html", viewPath);
};

/*console.log('*** contexte : '+request.url);

  console.log('*** year : '+q.query.year);
  console.log('*** month : '+q.query.month);

  console.log('*** host : '+q.host);
  console.log('*** pathname : '+q.pathnqme);
  console.log('*** search : '+q.sezarch);*/



/*fs.readdir("./", (err, files) => {
  files.forEach(file => {
    console.log(file);
  });
});*/