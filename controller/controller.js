var fs = require('fs');
var mime = require('mime-types'); // installation : npm install --save mime-types
var model = require('../model/model'); // sans le .js!
var viewHelper = require('../view/viewHelper');
var controllerHelper = require('./controllerHelper');

exports.handleRequest = function (requestParams, request, response) {

  //determine the next view to display
  var nextViewPath = controllerHelper.extractNextViewPath(request);
  // response MIME Type
  var reponseMime = mime.lookup(nextViewPath);
  
  // Testing if a file does not exist
  if (!viewHelper.viewExists("."+nextViewPath)) {
    controllerHelper.display404ErrorView(response);
    return;
  }
  console.log("*******File exists!!!");
  console.log('***>>>>>>>>>> nextViewPath : ' + nextViewPath);

  var contextObject = null;
  if (reponseMime.indexOf('text/html') >= 0) {
    //determine the operation 
    var operation = controllerHelper.extractUserOperation(request);
    console.log('***>>>>>>>>>> operation : ' + operation);
    
    // extract user information
    var userInfo = controllerHelper.getUserSentInfo(request);

    // Model
    contextObject = model.performOperation(requestParams, operation, userInfo, nextViewPath, request);
  }

  controllerHelper.displayView("."+nextViewPath, contextObject, response);
};

/*console.log('*** contexte : '+request.url);

  console.log('*** year : '+q.query.year);
  console.log('*** month : '+q.query.month);

  console.log('*** host : '+q.host);
  console.log('*** pathname : '+q.pathnqme);
  console.log('*** search : '+q.sezarch);*/

/*fs.readdir("./", (err, files) => {
  files.forEach(file => {
    console.log(file);
  });
});*/

  //response.write("<html><body><h1>Hello Querido ! </h1> <p> Date du jour : "+dt.myDateTime()+"</p></body></html>");
  //response.end();
  //response.end('Hello World - updated 3 !');