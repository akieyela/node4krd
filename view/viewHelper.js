var fs = require('fs');
var handlebars = require('handlebars'); // installation : npm install --save handlebars
var mime = require('mime-types'); // installation : npm install --save mime-types

exports.viewExists = function (path) {
  // Testing if a file does not exist
  try {
    return fs.existsSync(path);
  } catch (error) {
    return false;
  }
};

exports.generateView = function (response, contextObject, layout, view, viewSection1, viewSection2) {
  if (!this.viewExists(view)) {
    this.dispatch404Error(response);
    return;
  }

  // response MIME Type
  var reponseMime = mime.lookup(view);
  console.log('***>>>>>>>>>> reponseMime : ' + reponseMime);
  
  // templating avec Handlebar
  if (reponseMime.indexOf('text/html') >= 0) {

    var layoutContent = fs.readFileSync(layout).toString('utf-8');

    //compile layout template
    var dynamicViewScript = handlebars.compile('<script type="text/x-handlebars-template">' + layoutContent + '</script>');

    // register view section
    if(view!=null && view!=undefined) { 
      var viewContent = fs.readFileSync(view).toString('utf-8');
      if (view=="./index.html") {
        handlebars.registerPartial('view', "");
      } else {
        handlebars.registerPartial('view', viewContent);
      }
    }

    //section1 part
    if(viewSection1!=null && viewSection1!=undefined) {
      var section1Content = fs.readFileSync(viewSection1).toString('utf-8');
      handlebars.registerPartial('section1', section1Content);
    }

    //section2 part
    if(viewSection2!=null && viewSection2!=undefined) {
      var section2Content = fs.readFileSync(viewSection2).toString('utf-8');
      handlebars.registerPartial('section2', section2Content);
    }

    // generate view 
    var resultStaticView = dynamicViewScript(contextObject);

    // format response
    response.writeHead(200, { 'Content-Type': reponseMime });
    response.write(resultStaticView.substring(42, resultStaticView.length - 9));
    response.end();

  } else {
    // formattage de la réponse
    response.writeHead(200, { 'Content-Type': reponseMime });
    response.write(fs.readFileSync(view).toString('utf-8'));
    response.end();
  } 
};